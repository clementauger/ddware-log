# ddware-log

Module that help you to read and deal with demandware log system.

Start it, work your project, and let it displays the logs when they appears.

It gives color support, filtering, and timezone selection.

## Install

`npm i git+ssh://git@bitbucket.org:clementauger/ddware-log.git -g`


## Usage

```
  Usage: ddware-log [options]

  Options:
    -v, --vm <hostname>            VM hostname
    -a, --auth <auth>              Auth  cred
    -f, --files <filePattern>      File to read, a JS regexp without delimiter (/), it can use a placeholder `[=dod]` to input `date of the day`.
    -g, --grep <stringPattern>     String to grep, a JS regexp without delimiter (/)
    -e, --exclude <stringPattern>  String to exclude, a JS regexp without delimiter (/)
    -k                             Keep internal java trace
    -t                             Number of lines to tail from the log
    -z, --timezone <zoneName>      A valid moment/timezone to display your log dates. Defaults to Asia/Shanghai.
    --spin                         Show a spinner while loading the logs (Windows can t deal with it properly)

    -h, --help                     output usage information
    -V, --version                  output the version number
    
```

## Common usage

I usually drop a bat file under my project with something like this,

```
    ddware-log -v "VM HOST HERE" -a "USER:PWD" -f "(error|warn).+[=dod]"
```


## Internals

This software is a stupid http puller, text parser.


## Todo

- move auth to a config file per project.
- catch exception in network layer to avoid unexpected end of program. Many here in china.


## Notes

To debug, set the environment variable such

`SET DEBUG=dd* && ... your command`

`export DEBUG=dd* && ... your command`




Made with <3 to share it, your welcome to issue or propose updates.
