#!/usr/bin/env node

var pkg = require('./package.json');
var program = require('commander');
var hyperquest = require('hyperquest');
var byline = require('byline');
var moment = require('moment');
var momentTz = require('moment-timezone');
var path = require('path');
var chalk = require('chalk');
var Spinner = require('cli-spinner').Spinner;
var Fifo = require('fifo');
var async = require('async');
var _ = require('underscore');
var debug = require("debug")(pkg.name);

program
  .version('0.0.1')
  .option('-v, --vm <hostname>', 'VM hostname')
  .option('-a, --auth <auth>', 'Auth  cred')
  .option('-f, --files <filePattern>', 'File to read, a JS regexp without delimiter (/), it can use a placeholder `[=dod]` to input `date of the day`.')
  .option('-g, --grep <stringPattern>', 'String to grep, a JS regexp without delimiter (/)')
  .option('-e, --exclude <stringPattern>', 'String to exclude, a JS regexp without delimiter (/)')
  .option('-k', 'Keep internal java trace')
  .option('-t, --tail <tail>', 'Number of lines to tail from the log')
  .option('-z, --timezone <zoneName>', 'A valid moment/timezone to display your log dates. Defaults to Asia/Shanghai.')
  .option('--spin', 'Show a spinner while loading the logs (Windows can t deal with it properly)')
  .parse(process.argv);

var showSpin = !!program.spin;
var keepJTrace = !!program.K;
var vmHost = program.vm || '';
var vmAuth = program.auth || '';
var tail = program.tail || 30;
var timezone = program.timezone || 'Asia/Shanghai';
var baseUrl = 'https://'+vmHost+'.demandware.net';
var urlOpts = {
  auth: vmAuth,
  secureProtocol: false
};
program.files = program.files ? program.files.replace(/\[=dod]/, moment().format("MMDD")) : '.+';

var filePattern = new RegExp(program.files || '.+', 'i');
var grepPattern = new RegExp(program.grep || '.+', 'i');
var exPattern   = program.exclude ? new RegExp(program.exclude, 'i') : null;
var logsStorage = {};

console.log('');
console.log('\tFetching from\t\t' + baseUrl);
console.log('\tWith credentials\t' + vmAuth.split(':')[0]+':*******');
if(filePattern) console.log('\tOnly files\t\t' + filePattern.toString());
if(grepPattern) console.log('\tOnly string\t\t' + grepPattern.toString());
console.log('\tkeep Java trace\t\t' + (keepJTrace?'yes':'no'));
console.log('');


var spinner = new Spinner('loading logs.. %s');
spinner.setSpinnerString('|/-\\');

function keepGrepping (){
  catTheLogs(function(err){
    if(err) console.error(err);
    process.nextTick(keepGrepping);
  });
}
process.nextTick(keepGrepping);

function catTheLogs(then){
  if(showSpin)
    spinner.start();
  grepLogFiles(filePattern, function(err, files){
    if(err) return then(err);

    if(showSpin)
      spinner.stop(true);
    var filesCat = [];
    files.forEach(function(file){
      filesCat.push(function(done){
        catLogFile(file, function(line){
          line = line+'';
          grepLogLine(grepPattern, line, function(line){
            if(!(exPattern && line.match(exPattern)) && storeLogLine(file, line)){
              displayLogLine(file, line);
            }
          });
        }).on('end', done);
      });
    });
    async.parallelLimit(filesCat, 4, then)
  });
}

function displayLogLine (file, line){
  file = path.basename(file)
  var data = ''+line;
  data = data.replace(/(\s+)$/, '');
  var time = data.match(/^\[([^\]]+)\]/);
  if( time ) {
    data = data.replace(time[0], '');
    time = time[1];
    data = data.replace(/^\s+/, '');
    time = time.replace(/\s+GMT$/, '');
    var localTime = momentTz
      .tz(time, 'GMT').tz(timezone).format("dd DD MMM YYYY, HH:mm:ss");

    process.stdout.write('[' + chalk.green(localTime) + '] ');
    process.stdout.write('' + chalk.gray(file) + '');
    process.stdout.write('\n'+data);
    process.stdout.write('\n');
    process.stdout.write('\n');
  }
}

function grepLogLine (pattern, line, then){
  if(line.match(pattern)){
    then(line);
  }
}

function storeLogLine (file, line){
  if(logsStorage[file].lines.indexOf(line)===-1){
    logsStorage[file].lines.push(line);
    return true;
  }
  return false;
}

function catLogFile (file, onLine){
  var url = baseUrl+file;
  var request = hyperquest(url, urlOpts);
  request.on('error', function(d){
    console.error(d);
  });
  var r = byline(request);
  var buffer = '';
  var logLinesStack = new Fifo();
  r.on('data', function(data){
    data = ''+data;
    if(data.match(/^\[([^\]]+)\]/) && buffer.length ){
      logLinesStack.push(buffer);
      buffer = '';
      if(logLinesStack.length > tail ){
        logLinesStack.shift();
      }
    }
    if(keepJTrace){
      buffer+=data+'\n';
    }else if(!data.match(/^\s+at\s+/) ){
      buffer+=data+'\n';
    }
  });
  r.on('end', function () {
    if(buffer ){
      logLinesStack.push(buffer);
      if(logLinesStack.length > tail ){
        logLinesStack.shift();
      }
    }
    logLinesStack.toArray().forEach(onLine);
    logLinesStack.removeAll();
    r.end();
  });
  measureStreamTimings(url, r)
  return r;
}

function grepLogFiles (pattern, then){

  var lastLogFile;
  var logFiles = [];
  var url = baseUrl+'/on/demandware.servlet/webdav/Sites/Logs';
  var request = hyperquest(url, urlOpts);
  request.on('error', function(d){
    then(d)
  })
  var r = byline(request);
  r.on('data', function(data){
    var logFile = (data+'').match(/<a href="([^"]+log)"/);
    if(logFile){
      logFile = logFile[1];
      if(logFile.match(pattern)){
        if(!logsStorage[logFile]){
          logsStorage[logFile] = {
            mtime:'',
            lines:[]
          };
        }
        logFiles.push(logFile);
        lastLogFile = logFile;
      }
    }else if(!!lastLogFile){
      var logDate = (data+'').match(/<tt>([^<]+GMT)<\/tt>/);
      if(logDate){
        if(logsStorage[lastLogFile]){
          if(logsStorage[lastLogFile].mtime===logDate[1]){
            logFiles = _.without(logFiles, lastLogFile);
          }
        }else{
          logsStorage[lastLogFile].mtime = logDate[1];
        }
      }
    }
  });
  r.on('end', function () {
    logFiles.sort();
    then(null, logFiles);
    r.end();
  });

  measureStreamTimings(url, r)
}


function measureStreamTimings(url, S){
  var start = moment().unix();
  S.on('end', function(){
    debug('request '+url+' took ' + (moment().unix() - start)+' seconds' )
  })
}
